document.addEventListener("DOMContentLoaded", function () {

    (function initializeLottieObserver() {
        const lottiplayer = document.querySelectorAll("lottie-player");

        const callback = (entries) => {
            entries.forEach((entry) => {
                if (entry.isIntersecting) {
                    if (typeof entry.target.play === "function") {
                        entry.target.play();
                    }
                } else {
                    if (typeof entry.target.stop === "function") {
                        //only stop if autoplay is wanted
                        if(entry.target._lottie.autoplay) {
                            entry.target.stop();
                        }
                    }
                }
            });
        };

        const options = {
            root: null,
            rootMargin: '0px',
            threshold: 1,
        };

        const observer = new IntersectionObserver(callback, options);

        lottiplayer.forEach((player) => {
            observer.observe(player);
        });

    })();

});
