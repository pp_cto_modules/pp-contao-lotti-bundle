<?php

$GLOBALS['TL_LANG']['MSC']['detailpage']['pageTitle'] = 'More details';

// details page | text column (right side)
$GLOBALS['TL_LANG']['MSC']['detailpage']['articleId'] = 'Item number';
$GLOBALS['TL_LANG']['MSC']['detailpage']['articleDimension'] = 'Thickness x width x length (in mm):';
$GLOBALS['TL_LANG']['MSC']['detailpage']['articleColorRange'] = 'Color range:';
$GLOBALS['TL_LANG']['MSC']['detailpage']['articleSorting'] = 'Sorting:';

$GLOBALS['TL_LANG']['MSC']['detailpage']['viewInRoomPlaner'] = 'view in room planner';
$GLOBALS['TL_LANG']['MSC']['detailpage']['articleDataSheet'] = 'Product data sheet';
$GLOBALS['TL_LANG']['MSC']['detailpage']['viewMoreProducts'] = 'View more products';

// details page | anchor section
$GLOBALS['TL_LANG']['MSC']['detailpage']['furtherInformation'] = 'Further information:';

// details page | anchor section - link name
$GLOBALS['TL_LANG']['MSC']['detailpage']['section']['ProductInfo'] = 'Product information';
$GLOBALS['TL_LANG']['MSC']['detailpage']['section']['SkirtingBoard'] = 'Skirting board';
$GLOBALS['TL_LANG']['MSC']['detailpage']['section']['StairEdgingProfiles'] = 'Stair edging profiles';
$GLOBALS['TL_LANG']['MSC']['detailpage']['section']['CareProducts'] = 'Care products';
$GLOBALS['TL_LANG']['MSC']['detailpage']['section']['Downloads'] = 'Downloads';

// details page | anchor section - ID name
$GLOBALS['TL_LANG']['MSC']['detailpage']['sectionId']['productInfo'] = 'product-information';
$GLOBALS['TL_LANG']['MSC']['detailpage']['sectionId']['skirtingBoard'] = 'skirting-board';
$GLOBALS['TL_LANG']['MSC']['detailpage']['sectionId']['stairEdgingProfiles'] = 'stair-edging-profiles';
$GLOBALS['TL_LANG']['MSC']['detailpage']['sectionId']['careProducts'] = 'care-products';
$GLOBALS['TL_LANG']['MSC']['detailpage']['sectionId']['downloads'] = 'downloads';
