<?php

$GLOBALS['TL_LANG']['tl_content']['lotti_legend'] = 'Lotti';
$GLOBALS['TL_LANG']['tl_content']['lottifile'][0] = 'Lotti JSON';
$GLOBALS['TL_LANG']['tl_content']['lottifile'][1] = 'Bitte die Lotti Datei auswählen';

$GLOBALS['TL_LANG']['XPL']['lotti'][0][0] = 'Beispiel einfacher Loop';
$GLOBALS['TL_LANG']['XPL']['lotti'][0][1] = "{state: 'loop'}";
$GLOBALS['TL_LANG']['XPL']['lotti'][1][0] = 'Beispiel Autoplay mit Loop von Frame x bis Frame Y';
$GLOBALS['TL_LANG']['XPL']['lotti'][1][1] =  "{visibility:[0,1],state: 'autoplay',transition: 'onComplete'},{state: 'loop',frames: [250, 500]}";
