<?php

$GLOBALS['TL_DCA']['tl_content']['palettes']['lottifile'] = '{type_legend},type;{lotti_legend},lottifile,lottipreview;{expert_legend:hide},lottiActions;guests,cssID;{invisible_legend:hide},invisible,start,stop';

// Add fields to tl_module
$GLOBALS['TL_DCA']['tl_content']['fields']['lottifile'] = array
(
    'exclude' => true,
    'inputType' => 'fileTree',
    'eval' => array('multiple' => false, 'fieldType' => 'radio', 'files' => true, 'extensions' => 'json', 'mandatory' => true, 'submitOnChange' => true, 'tl_class' => 'w50 clr'),
    'sql' => "blob NULL",
);

// Add fields to tl_module
$GLOBALS['TL_DCA']['tl_content']['fields']['lottipreview'] = array
(
    'exclude' => true,
    'input_field_callback' => [\ppag\LottiBundle\Backend\Input::class, 'generateLottiPreview'],
    'eval' => array('tl_class' => 'w50'),
    'sql' => "blob NULL",
);

// Add fields to tl_module
$GLOBALS['TL_DCA']['tl_content']['fields']['lottiActions'] = array
(
    'inputType' => 'textarea',
    'explanation' => 'lotti',
    'eval' => array('tl_class' => 'clr', 'helpwizard' => true,'preserveTags' => true),
    'sql' => "text NULL",
);