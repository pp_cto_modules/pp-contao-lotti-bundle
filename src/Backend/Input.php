<?php

namespace ppag\LottiBundle\Backend;

use Contao\Backend;
use Contao\BackendTemplate;
use Contao\FilesModel;
use Symfony\Component\VarDumper\VarDumper;

class Input extends Backend
{

    public function generateLottiPreview($dca){

        $objTemplate = new BackendTemplate('be_lotti');
        $objLottiFile = FilesModel::findByPk($dca->activeRecord->lottifile);

        if($objLottiFile == NULL){
            return 'No Lotti File selected';
        }

        $objTemplate->lottiSRC = $objLottiFile->path;
        $objTemplate->lottiActions = $dca->activeRecord->lottiActions;
        $objTemplate->id = $dca->activeRecord->id;
        return $objTemplate->parse();
    }
}
